(function(factory) {
    /* global define */
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node/CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(window.jQuery);
    }
}(function($) {
  'use strict';

  function logIt(message) {
    var stack = new Error().stack;
    var caller = stack.split('\n')[2].trim();
    // console.log(caller + " : " + message);
  }

  var summernotePrettyprint = function (context) {
    var self = this;
    var options = context.options;
    var inToolbar = false;

    for (var idx in options.toolbar) {
      var buttons = options.toolbar[idx][1];
      if ($.inArray('visualLanguageEditor', buttons) > -1) {
        inToolbar = true;
        break;
      }
    }

    if (!inToolbar) { return; }

    var ui = $.summernote.ui;
    var $editor = context.layoutInfo.editor;
    var lang = options.langInfo;

    var prettyprintOpitions = {};

    options.prettyprint = $.extend(prettyprintOpitions, options.prettyprint);

    context.memo('button.visualLanguageEditor', function () {
      var button = ui.button({
        contents: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 17 17" width="14" height="14"><path d="M4 0h5.5v1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V4.5h1V14a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2z"/><path d="M9.5 3V0L14 4.5h-3A1.5 1.5 0 0 1 9.5 3z"/><path fill-rule="evenodd" d="M8.646 6.646a.5.5 0 0 1 .708 0l2 2a.5.5 0 0 1 0 .708l-2 2a.5.5 0 0 1-.708-.708L10.293 9 8.646 7.354a.5.5 0 0 1 0-.708zm-1.292 0a.5.5 0 0 0-.708 0l-2 2a.5.5 0 0 0 0 .708l2 2a.5.5 0 0 0 .708-.708L5.707 9l1.647-1.646a.5.5 0 0 0 0-.708z"/></svg>',
        tooltip: lang.button.tooltip,
        click: function (event) {
          self.show();
        }
      });

      return button.render();
    });

    this.show = function () {
      if (window.visualEditor) {
        // if exist then focus
        window.visualEditor.restore();
        window.visualEditor.focus();
      } else {
        // create new window
        const electron = require('electron');
        const BrowserWindow = electron.remote.BrowserWindow;
        // const ipc = electron.ipcRenderer;
        var current = electron.remote.getCurrentWindow();
        console.log(current);
        window.visualEditor = new BrowserWindow({
          width: 1280, height: 726,
          backgroundColor : 'black',
          parent: current,
          webPreferences: {
            nodeIntegration: true
          }
        });
        // load visual editor
        window.visualEditor.loadURL(`file://${__dirname}/../BCDP/src/index.html`);
        window.visualEditor.webContents.openDevTools();
        window.visualEditor.on('closed', function () {
          window.visualEditor = null
        });  
        
        const { ipcRenderer } = require('electron');
        var receiveCode = (event, message) => {
          console.log(message);
          self.insertToEditor({
            language: 'javascript',
            library: '',
            code: message
          });
          window.visualEditor.minimize();
        }
        ipcRenderer.off('aViewer', receiveCode);
        ipcRenderer.on('aViewer', receiveCode);
      }
    };

    this.showDialog = function () {
      logIt('');
      self.disableAddButton();
      self.$library.value = '';
      self.$code.value = '';

      return $.Deferred(function (deferred) {
        ui.onDialogShown(self.$dialog, function dialogShownCb () {
          context.triggerEvent('dialog.shown');
          self.$codeLanguage.focus();

          self.$addBtn.on('click', function addEmbedCb (event) {
            event.preventDefault();
            console.log(self.$codeLanguage.value);
            console.log(self.$library.value);
            console.log(self.$code.value);
            deferred.resolve({
              language: self.$codeLanguage.value,
              library: self.$library.value,
              code: self.$code.value
            });
          });
        });

        ui.onDialogHidden(self.$dialog, function dialogHiddenCb () {
          self.$addBtn.off('click');
          if (deferred.state() === 'pending') {
            deferred.reject();
          }
        });

        ui.showDialog(self.$dialog);
      });
    };

    this.createDialog = function ($container) {
      logIt('');


      /*var languages = [
        {
          name: "HTML",
          code: "html"
        }, {
          name: "CSS",
          code: "css"
        }, {
          name: "Javascript",
          code: "javascript"
        }, {
          name: "APACHE CONF",
          code: "apacheconf"
        }, {
          names: "ASP.Net",
          code: "aspnet"
        }, {
          name: "BASH",
          code: "bash"
        }, {
          name: "Basic",
          code: "basic"
        }, {
          name: "BATCH",
          code: "batch"
        }, {
          name: "C",
          code: "c"
        }, {
          name: "C#",
          code: "csharp"
        }, {
          name: "C++",
          code: "cpp"
        }, {
          name: "CoffeeScript",
          code: "coffeescript"
        }, {
          name: "Ruby",
          code: "ruby"
        }, {
          name: "Dart",
          code: "dart"
        }, {
          name: "Docker",
          code: "docker"
        }, {
          name: "Erlang",
          code: "erlang"
        }, {
          name: "Fortran",
          code: "fortran"
        }, {
          name: "Git",
          code: "git"
        }, {
          name: "Go",
          code: "go"
        }, {
          name: "GROOVY",
          code: "groovy"
        }, {
          name: "HASKELL",
          code: "haskell"
        }, {
          name: "HTTP",
          code: "http"
        }, {
          name: "Java",
          code: "java"
        }, {
          name: "JSON",
          code: "json"
        }, {
          name: "LATEX",
          code: "latex"
        }, {
          name: "LESS",
          code: "less"
        }, {
          name: "LUA",
          code: "lua"
        }, {
          name: "MAKEFILE",
          code: "makefile"
        }, {
          name: "Markdown",
          code: "markdown"
        }, {
          name: "MATLAB",
          code: "matlab"
        }, {
          name: "NGINX",
          code: "nginx"
        }, {
          name: "Objective-C",
          code: "objectivec"
        }, {
          name: "Pascal",
          code: "pascal"
        }, {
          name: "PERL",
          code: "perl"
        }, {
          name: "PHP",
          code: "php"
        }, {
          name: "Power Shell",
          code: "powershell"
        }, {
          name: "PROLOG",
          code: "prolog"
        }, {
          name: "Python",
          code: "python"
        }, {
          name: "Q",
          code: "q"
        }, {
          name: "JSX",
          code: "jsx"
        }, {
          name: "REST",
          code: "rest"
        }, {
          name: "SASS",
          code: "sass"
        }, {
          name: "SCSS",
          code: "scss"
        }, {
          name: "SCALA",
          code: "scala"
        }, {
          name: "SMALLTALK",
          code: "smalltalk"
        }, {
          name: "SQL",
          code: "sql"
        }, {
          name: "Stylus",
          code: "stylus"
        }, {
          name: "Swift",
          code: "swift"
        }, {
          name: "TEXTILE",
          code: "textile"
        }, {
          name: "Twig",
          code: "twig"
        }, {
          name: "Typescript",
          code: "typescript"
        }, {
          name: "YAML",
          code: "yaml"
        }
      ];
      */
      var languages = [
        "html", "css", "clike", "javascript", "apacheconf", "aspnet", "bash",
        "basic", "batch", "c", "csharp", "cpp", "coffeescript", "ruby", "dart",
        "docker", "erlang", "fortran", "git", "go", "groovy", "haskell", "http",
        "java", "json", "latex", "less", "lua", "makefile", "markdown", "matlab",
        "nginx", "objectivec", "pascal", "perl", "php", "powershell", "prolog",
        "python", "q", "jsx", "rest", "sass", "scss", "scala", "smalltalk", "sql",
        "stylus", "swift", "textile", "twig", "typescript", "yaml"
      ];

      var select = '<select class="form-control" id="form-control-code-language">';
      for (var i = 0; i < languages.length; ++i) {
        if (languages[i] == 'javascript') {
          select += '<option selected="selected">' + languages[i] + '</option>';
        } else {
          select += '<option>' + languages[i] + '</option>';
        }
      }
      select += '</select>';

      var dialogOption = {
        title: lang.dialogPrettyfi.title,
        // body: '<div class="form-group">' +
        body: '<div class="form-group" style="display: none;">' +
        '  <label>' + lang.dialogPrettyfi.selectLabel + '</label>' +
        select +
        '  <label>' + lang.dialogPrettyfi.libraryLabel + '</label>' +
        '  <input type="text" id="form-control-library" class="form-control"></input>' + 
        '</div>' +
        '<div class="form-group">' +
        '  <div id="form-group-check-run" class="form-check">' +
        '    <input class="form-check-input" type="radio" name="run" id="run-not-execute" value="not-execute">' +
        '    <label class="form-check-label" for="run-not-execute">실행 안함</label>' +
        '    <input class="form-check-input" type="radio" name="run" id="run-execute" value="execute" checked>' +
        '    <label class="form-check-label" for="run-execute">실행함</label>' +
        '  </div>' +
        '  <div>' +
        '    <button id="form-button-file" type="button" class="btn btn-info">파일 입력</button>' +
        '  </div>' +
        '  <label>' + lang.dialogPrettyfi.codeLabel + '</label>' +
        '  <textarea class="form-control" id="code" rows="7"></textarea>' +
        '</div>',
        footer: '<button href="#" id="btn-add" class="btn btn-primary">' + lang.dialogPrettyfi.button + '</button>',
        closeOnEscape: true
      };

      self.$dialog = ui.dialog(dialogOption).render().appendTo($container);
      self.$addBtn = self.$dialog.find('#btn-add');
      self.$codeLanguage = self.$dialog.find('#form-control-code-language')[0];
      self.$library = self.$dialog.find('#form-control-library')[0];
      console.log(self.$library);
      self.$code = self.$dialog.find('#code')[0];

      let select_code = document.getElementById('form-control-code-language')
      select_code.onchange = function (e) {
        if (e.target.value == 'javascript') {
          document.getElementById('form-group-check-run').style.display = 'block';
        }
        else {
          document.getElementById('form-group-check-run').style.display = 'none';
        }
      }

      let select_file = document.getElementById('form-button-file');
      select_file.onclick = function (e) {
        const fs = require('fs');
        const remote = require('electron').remote;
        const dialog = remote.require('electron').dialog;
        const dialog_js_options = {
          filters: [
            { name: 'All Files', extensions: ['*'] },
            { name: 'javascript', extensions: ['js'] },
          ],
        };
        dialog.showOpenDialog(dialog_js_options, function (filepaths) {
          console.log('filepath javascript: ', filepaths);
          if (!filepaths) {
            return;
          }

          let filepath = filepaths[0];

          try {
            let content = fs.readFileSync(filepath, 'utf-8');
            console.log(content);
            const selector_content = $('textarea#code')
            console.log(selector_content);
            selector_content.val(content);
            self.enableAddButton();
          } catch (err) {
            console.log('Error reading the file: ' + JSON.stringify(err));
          }
        });
      }
    };

    this.insertToEditor = function (options) {
      logIt('');
      var innerOptions = {
        code: '',
        library: '',
        language: ''
      };

      var _options = $.extend(innerOptions, options);

      let run_value = document.querySelector('input[name="run"]:checked').value;
      let select_code = document.getElementById('form-control-code-language');
      let code_value = select_code.options[select_code.selectedIndex].value;

      var $code = $('<code>');
      $code.html(_options.code.replace(/</g,"&lt;").replace(/>/g,"&gt;"));
      $code.addClass('language-' + _options.language);

      var $node;
      if (code_value == 'javascript' && run_value == 'execute') {
        var value_uniqe = Date.now();
        $node = $('<pre data-src="./lib/prism/prism-toolbar.js" data-label="run-script-' + value_uniqe + '">');
        $node.html($code);
        var run_script = `let modal = document.getElementById('run-modal');\n`
          + `if (modal.style.display == 'block') return;\n`
          + `modal.style.display = 'block';\n`
          + _options.code;
        run_script = run_script.replace(/\"/gi, "\'");
        console.log(run_script);

        var $button = $(`<template id="run-script-` + value_uniqe + `">` + `<button onclick="` + run_script + `">Execute</button></template>`);

        $node.append($button);
      }
      else {
        $node = $('<pre>');
        $node.html($code);
      }
      // var $node = $('<pre class="line-numbers">');

      context.invoke('editor.insertNode', $node[0]);
      var $tail = $('<p>');
      $tail.append('<br />');
      context.invoke('editor.insertNode', $tail[0]);

      $.each($code, function (index, element) {
        Prism.highlightElement(element);
      });

      let libraries = self.$library.value.split(',');
      let data;
      let settings;
      const fs = require('fs');
      const path = require('path');
      var temp_directory = path.join(__dirname, 'TempDirectory');
      if (!fs.existsSync(temp_directory)) {
        fs.mkdirSync(temp_directory);
      }
      let filepath_settings = path.join(temp_directory, 'settings.json');
      if (!fs.existsSync(filepath_settings)) {
        settings = {}
        data = JSON.stringify(settings, null, 4);
        fs.writeFileSync(filepath_settings, data);
      }
      data = fs.readFileSync(filepath_settings);
      settings = JSON.parse(data);
      settings['libraries'] = [];

      let scripts = document.getElementsByTagName("script");
      for (let i = 0; i < libraries.length; ++i) {
        let library = libraries[i].replace(/^\s+/,'').replace(/\s+$/,'');
        let have_to = true;
        for (let j = 0; j < scripts.length; ++j) {
          if (scripts[j].getAttribute('src') == library) {
            have_to = false;
            break;
          }
        }
        if (have_to) {
          let js = document.createElement("script");
 
          js.type = "text/javascript";
          js.src = library;
       
          document.body.appendChild(js);
        }
      }
      data = JSON.stringify(settings, null, 4);
      fs.writeFileSync(filepath_settings, data);
      console.log(settings);

      self.$library.innerHTML = '';
      self.$code.innerHTML = '';

    };

    this.enableAddButton = function() {
      logIt('');
      if (!!self.$codeLanguage.value && self.$code.value.length > 0) {
        self.$addBtn.attr("disabled", false);
      }
    };

    this.disableAddButton = function() {
      logIt('');
      self.$addBtn.attr("disabled", true);
    };

    this.init = function () {
      logIt('');
        $(self.$code).on('input', function (event) {
          self.enableAddButton();
          self.$code
        });
        $(self.$codeLanguage).on('change', function (event) {
          self.enableAddButton();
        });
    };

    this.initialize = function() {
      logIt('');
      var $container = options.dialogsInBody ? $(document.body) : $editor;
      self.createDialog($container);
    };

    this.destroy = function() {
      logIt('');
      ui.hideDialog(self.$dialog);
      self.$dialog.remove();
    };

    this.events = {
      'summernote.init': function(we, e) {
        self.init();
      }
    };
  };

  $.extend(true, $.summernote, {
    lang: {
      'en-US': {
        dialogPrettyfi: {
          title: 'Insert fragment code into editor',
          selectLabel: 'Choose your language',
          libraryLabel: 'Type library',
          codeLabel: 'Type your code',
          button: 'Insert code'
        },
        button: {
          tooltip: 'Insert code'
        }
      },
      'ko-KR': {
        dialogPrettyfi: {
          title: '코드 입력',
          selectLabel: '언어',
          libraryLabel: '라이브러리',
          codeLabel: '코드',
          button: '입력'
        },
        button: {
          tooltip: '코드'
        }
      },
      'pt-BR': {
        dialogPrettyfi: {
          title: 'Adicionar fragmento de código ao editor',
          selectLabel: 'Escolha a linguagem',
          codeLabel: 'Digite o seu código aqui',
          button: 'Inserir código'
        },
        button: {
          tooltip: 'Adicionar código'
        }
      }
    },
    plugins: {
      'visualLanguageEditor': summernotePrettyprint
    }
  });
}));
