bootstrap       3.4.1
jquery          3.5.1
summernote      0.8.18
codemirror      3.24 2.38
prettier        2.2.1
prism           1.22.0
epub-gen        0.1.0

npm install 하여 node module을 설치하기 전에
node_modules/epub-gen/lib/index.js를 백업
npm install 하여 node module을 설치한 후에
node_modules/epub-gen/lib/index.js를 덮어씌우기
