const { app, BrowserWindow, Menu } = require("electron");
const path = require("path");
const url = require("url");
const { ipcMain } = require('electron');

let win;

// https://stackoverflow.com/questions/48854265/why-do-i-see-an-electron-security-warning-after-updating-my-electron-project-t
process.env["ELECTRON_DISABLE_SECURITY_WARNINGS"] = "true";

function createWindow () {
  win = new BrowserWindow({
    width: 1280,
    height: 768,
    resizable: false,
    autoHideMenuBar: true,
    useContentSize: true,
    // webPreferences: {
    //     nodeIntegration: true,
    // },
  });

  win.loadURL(url.format({
    pathname: path.join(__dirname, "index.html"),
    protocol: "file:",
    slashes: true,
  }));

  ipcMain.on('aMain', (event, message) => {
    // /* Event emitter for sending asynchronous messages */
    // // event.sender.send('aViewer', message);
    win.webContents.send('aViewer', message);
  });
    
  // win.webContents.openDevTools();
  // if (process.argv.includes("devTools")) {
  //   win.webContents.openDevTools();
  // }
  win.setMenu(null);

  win.on("closed", () => {
    win = null;
  });
}

app.on("ready", createWindow)

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (win === null) {
    createWindow();
  }
});
