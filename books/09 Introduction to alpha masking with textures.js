var gfxScene = new GFX.Scene( { cameraPos:[0,0,15],
    canvasWidth: __run_root__.clientWidth,
    canvasHeight: __run_root__.clientWidth,
    containerID: 'run-root' } );

var num = 32;
var star = new Array(num);

initializeDemo();

animateScene();

function initializeDemo() {
    var starTexture = new THREE.ImageUtils.loadTexture("images/Star.jpg");

    for ( var i = 0; i < num; i++){
        var squareGeometry = new THREE.Geometry();
        squareGeometry.vertices.push(new THREE.Vector3(-1, -1, 0));
        squareGeometry.vertices.push(new THREE.Vector3( 1, -1, 0));
        squareGeometry.vertices.push(new THREE.Vector3( 1,  1, 0));
        squareGeometry.vertices.push(new THREE.Vector3(-1,  1, 0));
        squareGeometry.faces.push(new THREE.Face3(0, 1, 2));
        squareGeometry.faces.push(new THREE.Face3(0, 2, 3));
        squareGeometry.faceVertexUvs[0].push([
            new THREE.Vector2(0.0, 0.0),
            new THREE.Vector2(1.0, 0.0),
            new THREE.Vector2(1.0, 1.0)
        ]);
        squareGeometry.faceVertexUvs[0].push([
            new THREE.Vector2(0.0, 0.0),
            new THREE.Vector2(1.0, 1.0),
            new THREE.Vector2(0.0, 1.0)
        ]); 
        
        var squareMaterial = new THREE.MeshBasicMaterial({
            map:starTexture,
            transparent: true,
            combine: THREE.MixOperation,
            blending: THREE.AdditiveBlending,
            color:0xFFFFFF
        });

        var squareMesh = new THREE.Mesh(squareGeometry, squareMaterial);
        squareMesh.position.set(0.0, 0.0, 0.0);
        gfxScene.add(squareMesh);

        star[i] = new Object();
        star[i].angle = 0.0;
        star[i].dist  = (i / num) * 5.0;
        star[i].r     = Math.random();
        star[i].g     = Math.random();
        star[i].b     = Math.random();
        star[i].mesh  = squareMesh;
    }
}

function animateScene() {
    __run_ID__ = requestAnimationFrame(animateScene);

    var spin = 0;
    for ( var i = 0; i < num; i++ ) {
        spin += Math.PI * 2 / num;
        if (spin > (Math.PI*2))
                spin = 0;
        star[i].angle += i / num;
        star[i].dist  -= 0.01;
        if(star[i].dist < 0.0) {
            star[i].dist += 5.0;
            star[i].r    = Math.random();
            star[i].g    = Math.random();
            star[i].b    = Math.random();
        }
        
        star[i].mesh.matrix.setPosition(new THREE.Vector3(star[i].dist, 0, 0));
        var mr = new THREE.Matrix4();
        mr.makeRotationZ(spin);
        star[i].mesh.applyMatrix(mr);
        star[i].mesh.material.color.setRGB(star[i].r, star[i].g, star[i].b);
    }

    gfxScene.renderScene();
}
