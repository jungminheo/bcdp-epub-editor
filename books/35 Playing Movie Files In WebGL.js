var yRotation = 0.0;
var movieScreen;
var video, videoImage, videoImageContext, videoTexture;

var gfxScene = new GFX.Scene( {
    cameraPos: [0, 0, 30],
    controls: true,
    displayStats: false,
    clearColor:0x202020,
    canvasWidth: __run_root__.clientWidth,
    canvasHeight: __run_root__.clientWidth,
    containerID: 'run-root'
});

initializeDemo();

animateScene();

function initializeDemo() {
    video = document.createElement( 'video' );
    video.src = "images/Big_Buck_Bunny_Trailer_400p.ogv";
    video.load();
    video.play();
    video.loop = true;
    __run_player__ = video;

    videoImage = document.createElement( 'canvas' );
    videoImage.width = 720;
    videoImage.height = 400;

    videoImageContext = videoImage.getContext( '2d' );
    videoImageContext.fillStyle = '#000000';
    videoImageContext.fillRect( 0, 0, videoImage.width, videoImage.height );

    videoTexture = new THREE.Texture( videoImage );
    videoTexture.minFilter = THREE.LinearFilter;
    videoTexture.magFilter = THREE.LinearFilter;

    var videoMaterial = new THREE.MeshBasicMaterial( { map: videoTexture, overdraw: true, side:THREE.DoubleSide } );

    var movieGeometry = new THREE.PlaneGeometry( 21.6, 12, 4, 4 );
    movieScreen = new THREE.Mesh( movieGeometry, videoMaterial );
    movieScreen.position.set(0,0,0);
    gfxScene.add(movieScreen);
}

function animateScene() {
    __run_ID__ = requestAnimationFrame(animateScene);

    if ( video.readyState === video.HAVE_ENOUGH_DATA ) {
        videoImageContext.drawImage( video, 0, 0 );
        if ( videoTexture )
            videoTexture.needsUpdate = true;
    }

    yRotation += 0.01;
    movieScreen.rotation.set(0, yRotation, 0);

    gfxScene.renderScene();
}
