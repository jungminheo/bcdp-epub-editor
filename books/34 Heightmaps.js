var REPEAT_SIZE = 10;
var plane;

var gfxScene = new GFX.Scene( {
    cameraPos: [8, 10, 30],
    defaultLights: false,
    floorX: REPEAT_SIZE*3,
    floorZ: REPEAT_SIZE*3,
    floorRepeat: REPEAT_SIZE,
    controls:true,
    displayStats:false,
    canvasWidth: __run_root__.clientWidth,
    canvasHeight: __run_root__.clientWidth,
    containerID: 'run-root'
});

gfxScene.addLight( 'ambient', { color:0x333333, intensity : 1 });
var dirLight = gfxScene.addLight( 'directional', { color:0xffffff, intensity:1.0,  position:[10,10,20]});

initializeDemo();

animateScene();

function initializeDemo() {

    var lut = buildLut();

    var loader = new THREE.TextureLoader();
    var vScale = 5;

    loader.load('images/terrain-edged.png', function ( texture ) {
        this.uniforms = {
            uDirLightPos:   { type: "v3", value: dirLight.position },
            uDirLightColor: { type: "c", value: dirLight.color },
            uTexture:	    { type: "t", value: texture },
            uScale:	        { type: "f", value: vScale },
            uLut:           { type: "v3v", value: lut }
        };

        var shaderMaterial = new THREE.ShaderMaterial({
                uniforms: uniforms,
                vertexShader:   document.getElementById( 'vertexShader'   ).textContent,
                fragmentShader: document.getElementById( 'fragmentShader' ).textContent
            });

        var planeGeo = new THREE.PlaneGeometry( 20, 20, 513, 513 );
        plane = new THREE.Mesh(	planeGeo, shaderMaterial );
        plane.rotation.x = -Math.PI / 2;

        gfxScene.add(plane);
    });
}

function buildLut() {

    var SurfaceCover = [
        {name: "border", r: 0, g: 0, b: 0, limit: 1},
        {name: "water", r: 0, g: 0, b: 128, limit: 1},
        {name: "grass", r: 230, g: 223, b: 115, limit: 37},
        {name: "chapparal", r: 196, g: 191, b: 110, limit: 74},
        {name: "hardwood", r: 89, g: 133, b: 39, limit: 112},
        {name: "conifer", r: 37, g: 130, b: 96, limit: 149},
        {name: "tundra", r: 185, g: 211, b: 156, limit: 186},
        {name: "rock", r: 196, g: 204, b: 204, limit: 224},
        {name: "snow", r: 248, g: 251, b: 252, limit: 256}
    ];

    var k = 0;
    var lut = [];
    for (var n = 0; n < SurfaceCover.length; n++) {

        while (k < SurfaceCover[n].limit) {
            lut.push(new THREE.Vector3(SurfaceCover[n].r / 255.0, SurfaceCover[n].g / 255.0, SurfaceCover[n].b / 255.0));
            k++;
        }
    }

    return lut;
}

function animateScene() {
    __run_ID__ = requestAnimationFrame(animateScene);

    gfxScene.renderScene();
}
