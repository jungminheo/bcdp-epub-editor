var MAX_FOG = 0.5;
var MIN_FOG = 0.0;
var FOG_INCR = 0.025;
var CUBE_SIZE = 0.25;

var crateTexture;
var	yTranslation = CUBE_SIZE/2.0;
var	zTranslation = 0.0;
var	xPos, yPos;
var fogType = "exponential";
var fogDensity = 0.10;

var gfxScene = new GFX.Scene({
    cameraPos: [2,2,10],
    canvasWidth: __run_root__.clientWidth,
    canvasHeight: __run_root__.clientWidth,
    controls: true,
    displayStats: false,
    floorRepeat: 10,
    fogType: fogType,
    fogDensity: fogDensity,
    fogColor: 0xffffff,
    containerID: 'run-root'
});

initializeDemo();

animateScene();

function initializeDemo() {
    var boxGeometry = new THREE.BoxGeometry(CUBE_SIZE, CUBE_SIZE, CUBE_SIZE);

    crateTexture = new THREE.ImageUtils.loadTexture("images/Crate.jpg");

    var boxMaterial = new THREE.MeshLambertMaterial({
        map:crateTexture,
        side:THREE.DoubleSide
    });

    var boxMesh;
    for ( var x=-5; x<=5; x += 1) {
        boxMesh = new THREE.Mesh(boxGeometry, boxMaterial);
        boxMesh.position.set(x, yTranslation, zTranslation);
        gfxScene.add(boxMesh);
    }
    
    for ( var z=-5; z<=5; z += 1 ) {
        boxMesh = new THREE.Mesh(boxGeometry, boxMaterial);
        boxMesh.position.set(0.0, yTranslation, z);
        gfxScene.add(boxMesh);
    }

    document.addEventListener("keydown", onDocumentKeyDown, false);
}

function onDocumentKeyDown(event) {
    var keyChar = event.key;

    if (keyChar == '+') {
        if (fogDensity < MAX_FOG )
            fogDensity += FOG_INCR;
        else 
            fogDensity = MAX_FOG;
        gfxScene.addFog( { fogDensity: fogDensity } );
    }
    else if (keyChar == '-') {  
        if (fogDensity > MIN_FOG)
            fogDensity -= FOG_INCR;
        else
            fogDensity = 0;
        gfxScene.addFog( { fogDensity: fogDensity } );
    }
    else if (keyChar == 'r') {
        gfxScene.addFog( { fogColor: 0xff0000 } );
    }
    else if (keyChar == 'g') {
        gfxScene.addFog( { fogColor: 0x00ff00 } );
    }	
    else if (keyChar == 'b') {
        gfxScene.addFog( { fogColor: 0x0000ff } );
    }
    else if (keyChar == 'k') {
        gfxScene.addFog( { fogColor: 0x000000 } );
    }	
    else if (keyChar == 'w') {
        gfxScene.addFog( { fogColor: 0xffffff } );
    }

    event.stopPropagation();
}

function animateScene() {
    __run_ID__ = requestAnimationFrame(animateScene);
    
    gfxScene.renderScene();
}
