var	modelMesh;
var clock;
var mixer;

var gfxScene = new GFX.Scene( {
    cameraPos : [5, 5, 10],
    controls: true,
    displayStats: false,
    canvasWidth: __run_root__.clientWidth,
    canvasHeight: __run_root__.clientWidth,
    containerID: 'run-root'
    });

initializeDemo();

animateScene();

function initializeDemo() {
    gfxScene.renderer.setClearColor(new THREE.Color( 0x7f7f7f ), 1);
    loadCollada();
    clock = new THREE.Clock();
}

function  loadCollada() {
    var manager = new THREE.LoadingManager();
    var loader = new THREE.ColladaLoader(manager);

    loader.load('images/LoadCollada/avatar.dae', onLoadCollada);
}

function onLoadCollada(object) {
    mixer = new THREE.AnimationMixer( object );
    gfxScene.orbitControls[0].panUp(3.0);
    var dae = object.scene;

    dae.traverse( function ( child ) {
        if ( child instanceof THREE.SkinnedMesh ) {
            var clip = THREE.AnimationClip.parseAnimation( child.geometry.animation, child.geometry.bones );
            mixer.clipAction( clip, child ).play();
        }
    });

    modelMesh = dae;
    modelMesh.scale.set(3.33,3.33,3.33);
    modelMesh.rotateX(-Math.PI/2);
    gfxScene.add(modelMesh);
}

function animateScene() {
    __run_ID__ = requestAnimationFrame(animateScene);

    var delta = clock.getDelta();
    gfxScene.renderSceneAnimation(delta, mixer);
}
