var gfxScene = new GFX.Scene( {
    cameraPos : [0, 0, 0.01],
    fov: 90,
    near: 0.1,
    far: 100,
    controls: true,
    displayStats: false,
    canvasWidth: __run_root__.clientWidth,
    canvasHeight: __run_root__.clientWidth,
    containerID: 'run-root'
    });

initializeDemo();

animateScene();

function getTexturesFromAtlasFile( atlasImgUrl, tilesNum ) {
    var textures = [];
    for ( var i = 0; i < tilesNum; i ++ ) {
        textures[ i ] = new THREE.Texture();
    }

    var imageObj = new Image();
    imageObj.onload = function() {
        var canvas, context;
        var tileWidth = imageObj.height;

        for ( var i = 0; i < textures.length; i ++ ) {
            canvas = document.createElement( 'canvas' );
            context = canvas.getContext( '2d' );
            canvas.height = tileWidth;
            canvas.width = tileWidth;
            context.drawImage( imageObj, tileWidth * i, 0, tileWidth, tileWidth, 0, 0, tileWidth, tileWidth );
            textures[ i ].image = canvas
            textures[ i ].needsUpdate = true;
        }
    };
    imageObj.src = atlasImgUrl;

    return textures;
}

function initializeDemo() {
    var textures = getTexturesFromAtlasFile( "images/PanoramaCube/sun_temple_stripe.jpg", 6 );

    var materials = [];

    for ( var i = 0; i < 6; i ++ ) {
        materials.push( new THREE.MeshBasicMaterial( { map: textures[ i ] } ) );
    }

    var skyBox = new THREE.Mesh( new THREE.CubeGeometry( 1, 1, 1 ), materials );
    skyBox.applyMatrix( new THREE.Matrix4().makeScale( 1, 1, - 1 ) );
    gfxScene.add( skyBox );
}

function animateScene() {
    __run_ID__ = requestAnimationFrame(animateScene);

    gfxScene.renderScene();
}
