var gfxScene = new GFX.Scene( {
    cameraPos: [5, 2, 20],
    canvasWidth: __run_root__.clientWidth,
    canvasHeight: __run_root__.clientWidth,
    axesHeight: 10,
    controls: true,
    displayStats: false,
    containerID: 'run-root' });

var wiggle_count = 0;
var texture;
var paraGeom;
var paraMesh;

initializeDemo();

animateScene();

function initializeDemo() {
    texture = new THREE.ImageUtils.loadTexture("images/Tim.bmp");
    material = new THREE.MeshBasicMaterial( { map: texture, side:THREE.DoubleSide } );
    
    createMeshParametric();
}

function sineWave ( u, v ) {
    var x = u * 9 - 4.5;
    var y = v * 9 - 4.5;
    var z = Math.sin(u*Math.PI*2.0);
    
    return new THREE.Vector3(x, y, z);
}

function createMeshParametric() {
    paraGeom = new THREE.ParametricGeometry(sineWave, 45, 45);
    paraMesh = new THREE.Mesh( paraGeom, material);
    gfxScene.add(paraMesh);
}

function wiggleTheSurface() {

    vertices = paraGeom.vertices;

    var temp = vertices[0].z;
    for ( var i = 1; i < vertices.length; i++ ) {
        vertices[i-1].z = vertices[i].z;
    }

    vertices[vertices.length-1].z = temp;

    paraMesh.geometry.verticesNeedUpdate = true;
    }

function animateScene() {
    __run_ID__ = requestAnimationFrame(animateScene);

    wiggleTheSurface();
    
    gfxScene.renderScene();
}
