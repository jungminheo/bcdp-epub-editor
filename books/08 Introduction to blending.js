var gfxScene;

var xRotation = 0.0;
var yRotation = 0.0;

var xSpeed = 0.0;
var ySpeed = 0.0;

var zTranslation = 0.0;

var glassTexture;
var textureFilter = 0;

var ObjectMesh;

// "images/Glass.jpg"
// new THREE.SphereGeometry(2.0, 10, 10)
// new THREE.ConeGeometry(2.5, 1.5, 4)
// new THREE.DodecahedronGeometry(2.0, 3)
// new THREE.CylinderGeometry(1.5, 2.5, 2.5, 4)
// new THREE.BoxGeometry(2.0, 2.0, 2.0)
initializeThreejs(new THREE.BoxGeometry(2.0, 2.0, 2.0), "images/Glass.jpg", 0.75, [0,0,6]);

animateScene();

setKeyAction(66, function() {
    toggleBlending();
});

setKeyAction(38, function() {
    rotateX(-0.01);
});

setKeyAction(40, function() {
    rotateX(+0.01);
});

setKeyAction(37, function() {
    rotateY(-0.01);
});

setKeyAction(39, function() {
    rotateY(+0.01);
});

setKeyAction(33, function() {
    translateZ(-0.2);
});

setKeyAction(34, function() {
    translateZ(+0.2);
});

function initializeThreejs(geometry, image, opacity, cameraPos) {
    gfxScene = new GFX.Scene( { cameraPos: cameraPos,
        canvasWidth: __run_root__.clientWidth,
        canvasHeight: __run_root__.clientWidth,
        containerID: 'run-root' } );
        glassTexture = THREE.ImageUtils.loadTexture(image);

    var material = new THREE.MeshLambertMaterial({
        map: glassTexture,
        depthWrite: false,
        transparent: true,
        opacity: opacity,
        side:THREE.DoubleSide,
        combine: THREE.MixOperation
    });

    ObjectMesh = new THREE.Mesh(geometry, material);
    ObjectMesh.position.set(0.0, 0.0, zTranslation);
    gfxScene.add(ObjectMesh);
}

function setKeyAction(keyCode, callback) {
    function action (event) {
        if (event.keyCode == keyCode) {
            if (typeof callback == 'function') {
                callback();
            }
        }
        if (event.preventDefault) {
            event.preventDefault();
        }
        if (event.stopPropagation) {
            event.stopPropagation();
        }
        event.returnValue = false;
    }
    document.addEventListener("keydown", action, false);
}

function toggleBlending() {
    if (ObjectMesh.material.depthWrite == false) {
        ObjectMesh.material.depthWrite = true;
        ObjectMesh.material.opacity = 1.0;
        ObjectMesh.material.combine = THREE.MultiplyOperation;
    }
    else {
        ObjectMesh.material.depthWrite = false;
        ObjectMesh.material.transparent = true;
        ObjectMesh.material.opacity = 0.5;
        ObjectMesh.material.combine = THREE.MixOperation;
    }
    ObjectMesh.material.needsUpdate = true;
}

function rotateX(speed) {
    xSpeed += speed;
}

function rotateY(speed) {
    ySpeed += speed;
}

function translateZ(translation) {
    zTranslation += translation;
}

function animateScene() {
    xRotation += xSpeed;
    yRotation += ySpeed;
    ObjectMesh.rotation.set(xRotation, yRotation, 0.0);

    ObjectMesh.position.z = zTranslation;

    __run_ID__ = requestAnimationFrame(animateScene);

    gfxScene.renderScene();
}
